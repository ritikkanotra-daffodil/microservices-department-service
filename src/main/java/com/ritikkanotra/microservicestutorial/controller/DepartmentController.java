package com.ritikkanotra.microservicestutorial.controller;

import com.ritikkanotra.microservicestutorial.entity.Department;
import com.ritikkanotra.microservicestutorial.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/departments")
@Slf4j
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;


    @PostMapping("/")
    private Department saveDepartment(@RequestBody Department department) {
        log.info("Inside saveDepartment method of Controller layer");
        return departmentService.saveDepartment(department);
    }

    @GetMapping("/{id}")
    private Department fetchDepartmentById(@PathVariable("id") Long id) {
        log.info("Inside fetchDepartmentById method of Controller layer");
        return departmentService.fetchDepartmentById(id);
    }

}
