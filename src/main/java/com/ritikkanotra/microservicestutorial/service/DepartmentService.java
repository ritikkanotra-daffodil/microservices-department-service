package com.ritikkanotra.microservicestutorial.service;

import com.ritikkanotra.microservicestutorial.entity.Department;
import com.ritikkanotra.microservicestutorial.repository.DepartmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    public Department saveDepartment(Department department) {
        log.info("Inside saveDepartment method of Service layer");
        return departmentRepository.save(department);
    }

    public Department fetchDepartmentById(Long id) {
        log.info("Inside fetchDepartmentById method of Service layer");
        return departmentRepository.findById(id).get();
    }
}
