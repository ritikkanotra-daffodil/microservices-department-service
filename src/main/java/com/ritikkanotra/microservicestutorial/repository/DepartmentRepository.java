package com.ritikkanotra.microservicestutorial.repository;

import com.ritikkanotra.microservicestutorial.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {
}
